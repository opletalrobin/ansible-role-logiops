ansible-role-logiops
=========

This role can be used to install and configure the (logiops)[https://github.com/PixlOne/logiops] drivers on Linux

Requirements
------------

You need to have a logid configuration (logid.cfg) present under files/logid.cfg

Role Variables
--------------

- user - your user name
- logiops_source_repo_dir - where you would like the logiops repo to live
- upgrade_system - you can use this to decide whether to pull the latest git repo and repeat the install or not

Dependencies
------------

None

Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    - hosts: localhost
      vars:
        user: "robinopletal"
        logiops_source_repo_dir: "home/{{ user }}/Programs"
        upgrade_system: true

      roles:
         - role: ansible-role-logiops

License
-------

MIT
